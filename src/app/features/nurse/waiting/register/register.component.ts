import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationStart } from '@angular/router';
import { DateTime } from 'luxon';
import { NurseService } from '../../services/nurse.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { UserProfileService } from '../../../../core/services/user-profiles.service';
import { AxiosResponse } from 'axios';
import { LibService } from 'src/app/shared/services/lib.service';
import * as _ from 'lodash';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { NgxSpinnerService, Spinner } from "ngx-spinner";
import { NotificationService } from '../../../../shared/services/notification.service';
import { CanComponentDeactivate } from '../../../../core/guard/can-deactivate.guard'



@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegistersComponent implements OnInit, CanComponentDeactivate {
  // confirmModal?: NzModalRef;
  confirmModal?: NzModalRef<any, any> | undefined;
  query: any = '';
  dataSet: any[] = [];
  dataSetReview: any[] = [];
  dataSetAdmit: any[] = [];
  dataSetWaitingInfo: any[] = [];
  dataSetTreatement: any[] = [];
  dataSetPatientAllergy: any[] = [];
  loading = false;

  total = 0;
  pageSize = 20;
  pageIndex = 1;
  offset = 0;
  user_login_name: any;

  wardId: any;
  doctorId: any;
  wards: any = [];
  beds: any = [];
  doctors: any = [];
  queryParamsData: any;
  doctorBy: any;
  preDiag: any;
  chieft_complaint: any;
  panelsWard: any[] = [];
  panelsBed: any[] = [];
  bedId: any;
  isLoading: boolean = true;
  isVisible = false;
  userId: any;
  departmentId: any;
  isSaved = false;
  btnSaveIsVisible = true;
  selectedValue: any = null;
  //private routeSub: any;  // subscription to route observer

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private nurseService: NurseService,
    private spinner: NgxSpinnerService,
    private nzMessageService: NzMessageService,
    private modal: NzModalService,
    private libService: LibService,
    private userProfileService: UserProfileService,
    private notificationService: NotificationService


  ) {

    this.user_login_name = this.userProfileService.fname;
    this.userId = sessionStorage.getItem('userID');
    let jsonString: any = this.activatedRoute.snapshot.queryParamMap.get('data');
    const jsonObject = JSON.parse(jsonString);
    this.queryParamsData = jsonObject;
    console.log('queryParamsData : ', this.queryParamsData);


    // จะส่งค่า พารามส์  มา แบบปิด url
    // const receivedData:any  = this.router.getCurrentNavigation()?.extras.state;
    // console.log(receivedData);
    // this.queryParamsData = receivedData.data.queryParams;
    // this.currentState$ = this.activatedRoute.paramMap.pipe(
    //   map(() => window.history.state.data.queryParams)
    // );
  }

  ngOnInit() {

    //  Register to Angular navigation events to detect navigating away (so we can save changed settings for example)

    // this.routeSub = this.router.events.subscribe((event) => {
    //   if (event instanceof NavigationStart) {
    //     this.showConfirmModal('คำชี้แจ้ง', 'คุณยังไม่ได้บันทึกข้อมูล คุณแน่ใจว่าจะยกเลิกกระบวนการนี้ !');
    //   }
    // });
    this.getWaitingInfo();
    this.getReview();
    this.getAdmit();
    this.getWard();
    this.getDoctor();
    this.getTreatement();
    this.getPatientAllergy();
    this.assignCollapseWard();
  }

  // public ngOnDestroy() {
  //   this.routeSub.unsubscribe();
  // }
  /////////////เมธอด หลัก///////////ที่ต้องมี///////////////
  async canDeactivate() {
    if (!this.isSaved) {
      const confirm = await this.showConfirmModal('คำชี้แจ้ง', 'คุณยังไม่ได้บันทึกข้อมูล คุณแน่ใจว่าจะยกเลิกกระบวนการนี้ !');
      if (confirm) {
        this.isSaved = true;
        return true;
      } else {
        return false;
      }
    }
    return true;
  }

  showConfirmModal(title: string, content: string): Promise<boolean> {
    return new Promise<boolean>((resolve) => {
      this.confirmModal = this.modal.confirm({
        nzTitle: title,
        nzContent: content,
        nzOnOk: () => {
          resolve(true);
        },
        nzOnCancel: () => {
          resolve(false);
        }
      });
    });
  }

  hideSpinner() {
    setTimeout(() => {
      this.spinner.hide();
    }, 1000);
  }


  cancel(): void {
    this.nzMessageService.info('click cancel');
  }

  confirm(): void {
    this.nzMessageService.info('click confirm');
  }

  beforeConfirm(): Promise<boolean> {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve(true);
      }, 3000);
    });
  }

  //////////////////////

  async getReview() {
    this.spinner.show();
    try {
      const response = await this.nurseService.getReview(this.queryParamsData.an)
      const data = await response.data;
      this.dataSetReview = await data;  
      console.log('getReview : ', this.dataSetReview);
      this.isLoading = false;
      this.hideSpinner();
      // this.notificationService.notificationSuccess('คำชี้แจ้ง', 'กระบวนการทำงานสำเร็จ..', 'top');
    } catch (error: any) {
      console.log(error);
      this.hideSpinner();
      this.notificationService.notificationError('คำชี้แจ้ง', 'พบข้อผิดพลาด..' + error, 'top');

    }
  }

  async getTreatement() {
    try {
      const response = await this.nurseService.getTreatement(this.queryParamsData.an)
      const data = response.data;
      this.dataSetTreatement = await data;
      console.log('getTreatement : ', this.dataSetTreatement);
    } catch (error: any) {
      console.log(error);
      this.notificationService.notificationError('คำชี้แจ้ง', 'พบข้อผิดพลาด..' + error, 'top');
    }
  }

  async getPatientAllergy() {
    try {
      const response = await this.nurseService.getPatientAllergy(this.queryParamsData.an)
      const data = response.data;
      this.dataSetPatientAllergy = await data;
      console.log('getPatientAllergy : ', this.dataSetPatientAllergy);
    } catch (error: any) {
      console.log(error);
      this.notificationService.notificationError('คำชี้แจ้ง', 'พบข้อผิดพลาด..' + error, 'top');
    }
  }

  async getAdmit() {
    console.log('getAdmit');
    try {
      const response = await this.nurseService.getWaitingAdmit(this.queryParamsData.an);
      const data = await response.data;
      console.log(data);
      this.dataSetAdmit = await data;
      if(!this.dataSetAdmit[0]){
        this.notificationService.notificationError('คำชี้แจ้ง', 'ไม่พบข้อมูล Admit!', 'top');
        this.btnSaveIsVisible=false;
      }
      console.log('getAdmit : ', this.dataSetAdmit);
      setTimeout(() => {
        this.spinner.hide();
      }, 1000);
    } catch (error: any) {
      console.log(error);
      this.notificationService.notificationError('คำชี้แจ้ง', 'พบข้อผิดพลาด..' + error, 'top');
      setTimeout(() => {
        this.spinner.hide();
      }, 1000);

    }
  }
  async getWaitingInfo() {
    console.log('getWaitingInfo');
    try {
      const response = await this.nurseService.getWaitingInfo(this.queryParamsData.an)
      console.log(this.queryParamsData.an);
      const data = await response.data;
      this.dataSetWaitingInfo = await data;
      this.preDiag = this.dataSetWaitingInfo[0].pre_diag;
      this.doctorBy = this.dataSetWaitingInfo[0].admit_by
      if(!this.doctorBy || !this.preDiag){
        this.notificationService.notificationError('คำชี้แจ้ง', 'ไม่พบข้อมูลแพทย์ผู้สั่ง หรือ Diag text', 'top');
        this.btnSaveIsVisible=false;
      }
      console.log('getWaitingInfo : ', this.dataSetWaitingInfo);
      setTimeout(() => {
        this.spinner.hide();
      }, 1000);
    } catch (error: any) {
      console.log(error);
      this.notificationService.notificationError('คำชี้แจ้ง', 'พบข้อผิดพลาด..' + error, 'top');
      setTimeout(() => {
        this.spinner.hide();
      }, 1000);

    }
  }
  async getWard() {
    try {
      const response: AxiosResponse = await this.libService.getWard();
      const responseData: any = response.data;
      const data: any = responseData.data;
      this.wards = data;
      console.log('Ward : ', this.wards);
    } catch (error: any) {
      console.log(error);
      this.notificationService.notificationError('คำชี้แจ้ง', 'พบข้อผิดพลาด..' + error, 'top');

    }
  }

  async getDoctor() {
    try {
      const response: AxiosResponse = await this.libService.getDoctor();
      const responseData: any = response.data;
      const data: any = responseData.data;
      this.doctors = data;
      if (!_.isEmpty(data)) {
        // this.doctorId = .id;
      }
      console.log('Doctor : ', this.doctors);
    } catch (error: any) {
      console.log(error);
      this.notificationService.notificationError('คำชี้แจ้ง', 'พบข้อผิดพลาด..' + error, 'top');

    }
  }

  assignCollapseWard() {
    this.panelsWard = [
      {
        active: true,
        name: 'เลือกตึก',
        disabled: false
      },
    ];
  }

  assignCollapseBed() {
    this.panelsBed = [
      {
        active: true,
        name: 'เลือกเตียง',
        disabled: false
      },
    ];
  }

  setWard(data: any) {
    console.log(data);

    this.wardId = data.id;
    let wardName = data.name;
    this.departmentId = data.department_id;
    this.panelsWard = [
      {
        active: false,
        name: 'เลือกตึก : ' + wardName,
        disabled: false
      },
    ];
    console.log('wardId_now : ', this.wardId);
    this.bedId = '';
    this.getBed(this.wardId);
  }

  async getBed(wardId: any) {
    try {
      console.log('wardsId : ', wardId);
      const response: AxiosResponse = await this.libService.getBed(wardId);
      const responseData: any = response.data;
      const data: any = responseData.data;
      this.beds = data;
      this.assignCollapseBed();
      console.log('Beds : ', this.beds);
    } catch (error: any) {
      console.log(error);
      this.notificationService.notificationError('คำชี้แจ้ง', 'พบข้อผิดพลาด..' + error, 'top');


    }
  }

  setBed(data: any) {
    console.log('dataBeds : ', data);

    this.bedId = data.bed_id;
    let bedName = data.name;
    this.panelsBed = [
      {
        active: false,
        name: 'เลือกเตียง : ' + bedName,
        disabled: false
      },
    ];
    console.log('bedId : ', this.bedId);
  }

  logOut() {
    sessionStorage.setItem('token', '');
    return this.router.navigate(['/login']);
  }

  async saveRegister() {
    if(!this.wardId && !this.bedId){
      this.notificationService.notificationError('คำชี้แจ้ง', 'กรุณาเลือกตึกและเลือกเตียงให้ผู้ป่วย.', 'top');
    }else{
      console.log(this.wardId);
      this.isSaved = true;
      this.spinner.show();
      let info: any = {
        "patient": this.dataSetWaitingInfo,
        "review": this.dataSetReview,
        "treatment": this.dataSetTreatement,
        "admit": this.dataSetAdmit,
        "bed": [
          {
            "bed_id": this.bedId,
            "status": "Used"
          }
        ],
        "ward": [
          {
            "ward_id": this.wardId,
          }
        ],
        "doctor": [
          { "user_id": this.selectedValue }
        ],
        "department": [
          { "department_id": this.departmentId }
        ],
        "patient_allergy": this.dataSetPatientAllergy
      }
      console.log(info);
      try {
        const response = await this.nurseService.saveRegister(info);
        console.log(response);
        if (response.status === 200) {
          this.hideSpinner();
          this.notificationService.notificationSuccess('คำชี้แจ้ง', 'บันทึกสำเร็จ..', 'top');
          this.isSaved = true;
          setTimeout(() => {
            this.navigateWaiting();
          }, 2000);
  
        }
  
      } catch (error) {
        console.log(error);
        this.hideSpinner();
        this.notificationService.notificationError('คำชี้แจ้ง', 'พบข้อผิดพลาด..' + error, 'top');
      }
    }
  }



  showModal(): void {
    if (!_.isEmpty(this.wardId) && !_.isEmpty(this.bedId)) {
      this.showConfirm('แจ้งเตือน', 'ต้องการบันทึกข้อมูล ใช่หรือไม่ ?');
    } else {
      this.notificationService.notificationWaring('คำชี้แจ้ง', 'ward & bed is empty please select ward and bed..', 'top');

    }
  }

  handleOk(): void {
    this.isVisible = false;
    this.saveRegister();

  }

  handleCancel(): void {
    console.log('Cancel Save');
  }

  showConfirm(title: any, content: any): void {
    this.confirmModal = this.modal.confirm({
      nzTitle: title,
      nzContent: content,
      // nzOnOk: () => this.saveRegister(),
      nzOnOk: () => this.handleOk(),
      // nzOnOk: () =>
      //   new Promise((resolve, reject) => {
      //     setTimeout(Math.random() > 0.5 ? resolve : reject, 1000);
      //   }).catch(() => console.log('Oops errors!')),
      nzOnCancel: () => this.handleCancel(),
    });
  }

  showDeleteConfirm(): void {
    this.confirmModal = this.modal.confirm({
      nzTitle: 'Are you sure delete this task?',
      nzContent: '<b style="color: red;">Some descriptions</b>',
      nzOkText: 'Yes',
      nzOkType: 'primary',
      nzOkDanger: true,
      nzOnOk: () => console.log('OK'),
      nzCancelText: 'No',
      nzOnCancel: () => console.log('Cancel')
    });
  }

  async navigateWaiting() {
    // const confirm = await this.showConfirmModal('คำชี้แจ้ง', 'คุณยังไม่ได้บันทึกข้อมูล คุณแน่ใจว่าจะยกเลิกกระบวนการนี้ !');

    // if (confirm) {
    //   this.isSaved = true;
    this.router.navigate(['/nurse/waiting']);
    //   }
  }

  /////////////////get data  connter here/////////////////
  async getAdmitNew() {
    this.spinner.show();
    let an = '66000124';

    try {

      const response = await this.nurseService.getAdmit(an);
      console.log(response);
      const data: any = response.data;

      this.dataSet = data.map((v: any) => {
        const date = v.admit_date ? DateTime.fromISO(v.admit_date).setLocale('th').toLocaleString(DateTime.DATE_MED) : '';
        const time = v.admit_time ? DateTime.fromISO(v.admit_date).setLocale('th').toLocaleString(DateTime.TIME_24_SIMPLE) : '';
        v.admit_date = date;
        // v.admit_time = time;
        return v;
      });

      setTimeout(() => {
        this.spinner.hide();
      }, 1000);
    } catch (error: any) {
      console.log(error);
      setTimeout(() => {
        this.spinner.hide();
      }, 1000);
    }
  }

}
