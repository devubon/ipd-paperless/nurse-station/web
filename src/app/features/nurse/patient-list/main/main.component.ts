import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { NurseService } from '../../services/nurse.service';
import { DateTime } from 'luxon';
import { AxiosResponse } from 'axios';
import { LibService } from '../../../../shared/services/lib.service';
import { UserProfileService } from '../../../../core/services/user-profiles.service';
import * as _ from 'lodash';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css'],
})
export class MainComponent implements OnInit {
  admitId: any;
  doctorID: any;
  dataWard: any;
  wardName: any;
  wards: any = [];
  query: any = '';
  dataSet: any[] = [];
  loading = false;
  doctors: any = [];

  total = 0;
  pageSize = 20;
  pageIndex = 1;
  offset = 0;
  user_login_name: any;
  isVisible = false;
  isOkLoading = false;
  inputValue?: string;
  modalTitle: string = 'ระบุแพทย์เจ้าของไข้';
  selectedValue: any = null;
  confirmModal?: NzModalRef;
  constructor(
    private router: Router,
    private nurseService: NurseService,
    private libService: LibService,
    private message: NzMessageService,
    private modal: NzModalService,
    private userProfileService: UserProfileService,
    private notificationService: NotificationService
  ) {}

  async ngOnInit() {
    // this.user_login_name  =  this.userProfileService.user_login_name;
    this.user_login_name = sessionStorage.getItem('userLoginName');

    console.log(this.user_login_name);
    this.getWard();
    this.getDoctor();
  }

  async handleOk() {
    this.isOkLoading = true;
    setTimeout(async () => {
      console.log(this.selectedValue);
      let data: any = {
        doctor_id: this.selectedValue,
      };
      const response: AxiosResponse = await this.nurseService.changeDoctor(
        data,
        this.admitId
      );
      this.admitId = null;
      this.selectedValue = null;
      this.isVisible = false;
      this.isOkLoading = false;
      this.getWard();
    }, 3000);
  }

  handleCancel(): void {
    this.admitId = null;
    this.selectedValue = null;
    this.isVisible = false;
  }

  async getDoctor() {
    console.log('getDoctor');
    try {
      const response: AxiosResponse = await this.libService.getDoctor();
      const responseData: any = response.data;
      const data: any = responseData.data;
      this.doctors = data;
      console.log('Doctor : ', this.doctors);
    } catch (error: any) {
      console.log(error);
      this.notificationService.notificationError(
        'คำชี้แจ้ง',
        'พบข้อผิดพลาด..' + error,
        'top'
      );
    }
  }

  doSearch() {
    this.getAdmitActive();
  }
  logOut() {
    sessionStorage.setItem('token', '');
    return this.router.navigate(['/login']);
  }

  onPageIndexChange(pageIndex: any) {
    this.offset = pageIndex === 1 ? 0 : (pageIndex - 1) * this.pageSize;

    this.getAdmitActive();
  }

  onPageSizeChange(pageSize: any) {
    this.pageSize = pageSize;
    this.pageIndex = 1;

    this.offset = 0;

    this.getAdmitActive();
  }

  refresh() {
    this.query = '';
    this.pageIndex = 1;
    this.offset = 0;
    this.getAdmitActive();
  }

  async onSelectWard(event: any) {
    console.log(event);

    this.dataWard = event;
    this.getAdmitActive();
    this.wardName = event.name;
  }

  async getAdmitActive() {
    const messageId = this.message.loading('Loading...').messageId;
    try {
      const _limit = this.pageSize;
      const _offset = this.offset;
      const response = await this.nurseService.getAdmitActive(
        this.dataWard.id,
        this.query,
        _limit,
        _offset
      );
      const data: any = response.data;
      console.log(data);

      this.total = data.total || 1;
      this.dataSet = data.data.map((v: any) => {
        const date = v.admit_date
          ? DateTime.fromISO(v.admit_date)
              .setLocale('th')
              .toLocaleString(DateTime.DATE_MED)
          : '';
        v.admit_date = date;
        const time = v.admit_time
          ? DateTime.fromFormat(v.admit_time, 'HH:mm:ss').toFormat('HH:mm')
          : '';
        v.admit_time = time;
        return v;
      });
      this.message.remove(messageId);
    } catch (error: any) {
      this.message.remove(messageId);
      this.message.error(`${error.code} - ${error.message}`);
    }
  }

  async getWard() {
    try {
      const response: AxiosResponse = await this.libService.getWard();
      const responseData: any = response.data;
      const data: any = responseData.data;
      this.wards = data;
      if (!_.isEmpty(data)) {
        const selectedWard: any = data[0];
        this.dataWard = selectedWard;
        this.wardName = selectedWard.name;
      }
      await this.getAdmitActive();
    } catch (error: any) {
      console.log(error);
    }
  }

  async getWardName(wardId: any) {
    const rsWardName = await this.libService.getWardName(wardId);
    console.log(rsWardName.data.data[0]);
    return rsWardName.data.data[0];
  }

  changeWard(data: any) {
    console.log(data);
    let dk: any = { admit_id: data };
    let jsonString = JSON.stringify(dk);
    console.log(jsonString);
    this.router.navigate(['/nurse/patient-list/change-ward'], {
      queryParams: { data: jsonString },
    });
  }
  async nurseNote(data: any) {
    // console.log(data);
    // let dk: any = { data };
    // console.log('dk : ', dk);

    // let jsonString = JSON.stringify(dk);
    // console.log(jsonString);
    sessionStorage.setItem('itemPatientInfo', JSON.stringify(data));

    console.log('itemPatientInfo data : ', data);

    await this.router.navigate(['/nurse/patient-list/nurse-note']); //, { queryParams: { data: jsonString } });
  }

  async changeDoctor(data: any) {
    console.log(data);
    this.admitId = await data.id;
    this.selectedValue = await data.doctor_id;
    console.log(this.selectedValue);

    this.isVisible = true;
  }

  async deleteAdmit(id: any) {
    console.log(id);
    this.confirmModal = this.modal.confirm({
      nzTitle: 'คำเดือน ?',
      nzContent:
        'คุณต้องการลบ จริงหรือไม่ ?',
      nzOnOk: async (params: any) => {
        let data: any = {
          is_active: false,
        };
        const response: AxiosResponse = await this.nurseService.changeDoctor(
          data,
          id
        );
        this.getWard();
      },     
    });
  }
}
