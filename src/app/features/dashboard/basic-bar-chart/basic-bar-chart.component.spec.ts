import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BasicBarChartComponent } from './basic-bar-chart.component';

describe('BasicBarChartComponent', () => {
  let component: BasicBarChartComponent;
  let fixture: ComponentFixture<BasicBarChartComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BasicBarChartComponent]
    });
    fixture = TestBed.createComponent(BasicBarChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
